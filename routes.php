<?php

Route::prefix('sso')
    ->namespace('Dottystyle\LaravelSSO\IdentityProvider\Controllers')
    ->group(function () {
        Route::get('token', 'ServerController@token')->name('user')->middleware('sso.token_auth:0');
        Route::get('compare', 'ServerController@compare')->name('compare')->middleware('sso.token_auth:0');
        Route::get('logout', 'ServerController@logout')->name('logout')->middleware('web', 'sso.token_auth');
    });