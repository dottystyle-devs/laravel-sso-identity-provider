<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Token;

use Dottystyle\LaravelSSO\IdentityProvider\Contracts\TokenStore as StoreContract;
use Dottystyle\LaravelSSO\TokenInterface;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Carbon\Carbon;

class Store implements StoreContract
{
    /**
     * @var string
     */
    protected $model;

    /**
     * @var int
     */
    protected $lifetime;

    /**
     * Create new instance of the token store.
     * 
     * @param string|\Illuminate\Database\Eloquent\Model $model
     * @param int $lifetime The lifetime of the token in minutes.
     */
    public function __construct($model, int $lifetime)
    {
        $this->model = $model instanceof Eloquent ? $model :  (new $model);
        $this->lifetime = $lifetime;
    }

    /**
     * Create a token with the given parameters.
     * 
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param array $params (optional)
     * @return \Dottystyle\LaravelSSO\TokenInterface $token
     */
    public function create(Authenticatable $user, array $params = [])
    {    
        do {
            // Try generating an id until we have a unique one
            $id = Str::random(60);
        } while ($this->model->newQuery()->find($id));

        $token = $this->model->newInstance($params);
        $token->{$token->getKeyName()} = $id;
        $token->user_id = $user->getAuthIdentifier();
        $token->expired_at = Carbon::now()->addMinutes($this->lifetime);

        return $token;
    }

    /**
     * Save the given token.
     * 
     * @param \Dottystyle\LaravelSSO\TokenInterface $token
     * @return bool
     */
    public function put(TokenInterface $token)
    {
        // Save the token here since the tokens are already given a unique id on create method. 
        $token->save();

        return true;
    }

    /**
     * Get an SSO token by id.
     * 
     * @param string $id
     * @return \Dottystyle\LaravelSSO\TokenInterface
     */
    public function get($id)
    {
        $token = $this->model->newQuery()->find($id);

        return (isset($token) && !$token->expired()) ? $token : null;
    }

    /**
     * Destroy/delete token.
     * 
     * @param \Dottystyle\LaravelSSO\TokenInterface $token
     * @return bool
     */
    public function destroy(TokenInterface $token)
    {
        return (bool) $this->model::destroy($token->getTokenId());
    }
}