<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Token;

use Dottystyle\LaravelSSO\TokenInterface;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Carbon\Carbon;

class Model extends Eloquent implements TokenInterface
{
    /**
     * @var string 
     */
    protected $table = 'sso_tokens';

    /**
     * @var boolean Disable incrementing since we generate the ids
     */
    public $incrementing = false;

    /**
     * @var array
     */
    public $dates = [
        'expired_at'
    ];

    /**
     * Get the token identifier.
     * 
     * @return string
     */
    public function getTokenId()
    {
        return $this->getKey();
    }

    /**
     * Get the user from token. 
     * 
     * @return mixed
     */
    public function getUserId()
    {
        return (int) $this->user_id;
    }

    /**
     * Get the expiration date of the token.
     * 
     * @return \Carbon\Carbon
     */
    public function remainingMinutes()
    {
        return max(0, $this->expired_at->diffInMinutes(Carbon::now()));
    }

    /**
     * Determine whether the SSO is expired or not.
     * 
     * @return boolean
     */
    public function expired()
    {
        return ! $this->expired_at->isFuture();
    }
}