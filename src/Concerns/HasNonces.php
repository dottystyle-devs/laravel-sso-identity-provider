<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Concerns;

use Illuminate\Contracts\Cache\Repository;

trait HasNonces
{
    /**
     * @var \Illuminate\Contracts\Cache\Repository
     */
    protected $nonceRepository;

    /**
     * @var string
     */
    protected $noncePrefix;

    /**
     * @var int 
     */
    protected $nonceDuration = 1 * 60;

    /**
     * Set the cache repository for nonces.
     * 
     * @param \Illuminate\Contracts\Cache\Repository $store
     * @return void
     */
    public function setNonceRepository(Repository $repo)
    {
        $this->nonceRepository = $repo;
    }

    /**
     * Get the cache repository for nonces.
     * 
     * @return \Illuminate\Contracts\Cache\Repository
     */
    public function getNonceRepository(Repository $repo)
    {
        return $this->nonceRepository;
    }

    /**
     * Store a nonce to the repository.
     * 
     * @param string $nonce
     * @param mixed $data (optional)
     * @return void
     */
    public function putNonce($nonce, $data = null)
    {
        // Use the current date and IP address of the 
        $this->nonceRepository->put($this->noncePrefix.$nonce, $data, $this->nonceDuration);
    }

    /**
     * Verify whether the given nonce already exists or not.
     * 
     * @param string $nonce
     * @return bool
     */
    protected function nonceExists($nonce)
    {
        return ! is_null($this->nonceRepository->get($this->noncePrefix.$nonce));
    }

}