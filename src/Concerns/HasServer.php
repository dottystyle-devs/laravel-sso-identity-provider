<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Concerns;

trait HasServer
{
    /**
     * Get the SSO identity provider server.
     * 
     * @return \Dottystyle\LaravelSSO\IdentityProvider\Server
     */
    public function server()
    {
        return app('sso.server');
    }
}