<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Concerns;

use Dottystyle\LaravelSSO\IdentityProvider\Middleware\IdentifyServiceProvider;
use Dottystyle\LaravelSSO\Exceptions\AuthenticationException;
use Dottystyle\LaravelSSO\Exceptions\SSOException;
use Dottystyle\LaravelSSO\RequestUtils;
use Dottystyle\LaravelSSO\ErrorCodes;
use Illuminate\Http\Request;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\Auth;
use Exception;

/**
 * Contains methods/endpoints for the SSO authentication.
 */
trait HasServerEndpoints
{
    use HasServer;

    /**
     * Initialize the SSO controller.
     * 
     * @return void
     */
    protected function initializeEndpoints() 
    {
        $this->middleware(IdentifyServiceProvider::class);
    }

    /**
     * Get the token stat.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response $response
     */
    public function token(Request $request)
    {
        try {
            $server = $this->server();
            
            $server->verifyTokenStat($request->input('sig'), $salt = $request->input('salt'));

            return response()->json([
                'data' => $server->getTokenStat(),
                'sig' => $server->signTokenStatSuccess($server->getToken()->getTokenId(), $salt)
            ]);
        } catch (AuthenticationException $e) {
            return response()->json([], 401);
        }
    }

    /**
     * Compare the token passed by service provide to current token held by the identity provider.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * 
     * @throws \Illuminate\Session\TokenMismatchException
     */
    public function compare(Request $request)
    {
        $tokenId = $request->input('sp_token');

        try {
            $this->server()->verifyTokenCompare(
                $request->input('sig'), $tokenId, $request->input('salt')
            );

            if ($tokenId && hash_equals($tokenId, $this->server()->getToken()->getTokenId())) {
                if (! $request->has('success')) {
                    return response()->json(['result' => true]);
                }
                
                return $this->createCompareRedirectResponse($request, $tokenId);
            } elseif (! $request->has('failed')) {
                return response()->json(['error' => ErrorCodes::TOKEN_MISMATCH], 419);
            }

            return $this->createCompareRedirectResponse($request, $tokenId, ErrorCodes::TOKEN_MISMATCH);
        } catch (SSOException $e) {
            if ($request->has('failed')) {
                return $this->createCompareRedirectResponse($request, $tokenId, $e->getErrorCode());
            }

            throw $e;
        }
    }

    /**
     * Create a redirect response for token comparison.
     * 
     * @param \Illuminate\Http\Request $request
     * @param string $tokenId
     * @param mixed $errorCode (optional)
     * @return \Illuminate\Http\Response
     */
    protected function createCompareRedirectResponse(Request $request, $tokenId, $errorCode = null) 
    {
        $url = $errorCode ? $request->input('failed') : $request->input('success');

        $params = [
            'sp_token' => $tokenId,
            'salt' => $salt = RequestUtils::salt(),
            'sig' => $this->server()->signTokenCompare($tokenId, $salt),
        ];

        if (isset($errorCode)) {
            $params['error'] = $errorCode;
        }

        return redirect(RequestUtils::appendUrlParams($url, $params));
    }

    /**
     * Logout the SSO user (delete token and logout session)
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response 
     */
    public function logout(Request $request)
    {
        $this->server()->verifyLogout($request->input('sig'));

        // Return to our home if no return url was sent by the service provider.
        $returnUrl = $request->input('return_url') ?? route('home');

        if (Auth::check()) {
            // Token will be invalidated when logout event is triggered.
            Auth::logout();
        } else {
            $this->server()->logout();
        }

        return redirect($returnUrl);
    }
}