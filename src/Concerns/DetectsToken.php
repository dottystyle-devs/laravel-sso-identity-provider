<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Concerns;

use Dottystyle\LaravelSSO\IdentityProvider\Server;
use Illuminate\Http\Request;

trait DetectsToken
{
    /**
     * Get the token from request.
     * 
     * @param \Dottystyle\LaravelSSO\IdentityProvider\Server $server
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    protected function setTokenFromRequest(Server $server, Request $request)
    {
        $server->setTokenById($request->cookies->get($server->getTokenName()) ?? $request->input('token'));
    }
}