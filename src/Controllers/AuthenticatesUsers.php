<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Controllers;

use Dottystyle\LaravelSSO\IdentityProvider\Concerns\HasServer;
use Dottystyle\LaravelSSO\IdentityProvider\Middleware\IdentifyServiceProvider;
use Dottystyle\LaravelSSO\RequestUtils;
use Dottystyle\LaravelSSO\Exceptions\SSOException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Contains controller methods to authenticate users via SSO.
 */
trait AuthenticatesUsers
{
    use HasServer;

    /**
     * @var string
     */
    protected $loginMethod = 'login';

    /**
     * @var string
     */
    protected $cnonce;

    /**
     * Set the SSO-related functionalities.
     * Must be called on the controller constructor.
     * 
     * @return void
     */
    protected function setupSSO()
    {
        $this->middleware(IdentifyServiceProvider::class)->only('showSSOLoginFormAction', 'ssoLogin');
    }

    /**
     * Show the login form.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function showSSOLoginFormAction(Request $request)
    {
        try {
            $this->validateServiceProviderLogin($request);

            // If the request accepts image and user is not logged in, do not prompt the user to login
            // but return an error response to the service provider instead.
            if (Auth::check()) {
                return $this->authenticatedServiceProviderLogin($request, Auth::user());
            } elseif (RequestUtils::acceptsImage($request) || $request->input('silent')) {
                return $this->failedServiceProviderLogin($request, false);
            }

            return $this->showSSOLoginForm($request, [
                'action' => route('sso.login', $request->query())
            ]);
        } catch (SSOException $e) {
            return view('sso::login-error', ['exception' => $e]);
        }
    }

    /**
     * Show the SSO login form.
     * 
     * @param \Illuminate\Http\Request $request
     * @param array $parameters
     * @return \Illuminate\Http\Response
     */
    protected function showSSOLoginForm(Request $request, array $variables)
    {
        $template = property_exists($this, 'ssoLoginForm') ? $this->ssoLoginForm : 'auth.login';

        return view($template, $variables);
    }

    /**
     * Attempt to login the user requested by the service provider.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function ssoLogin(Request $request)
    {
        $this->validateServiceProviderLogin($request);

        // Forward login to the actual login method.
        $response = $this->{$this->loginMethod}($request);

        return Auth::check() ? $this->authenticatedServiceProviderLogin($request, Auth::user()) : $response;
    }

    /**
     * Determine whether the login has been requested by the service provider.
     * 
     * @param \Illuminate\Http\Request $request
     * @return void
     * 
     * @throws \Dottystyle\LaravelSSO\Exceptions\SSOException
     */
    protected function validateServiceProviderLogin(Request $request)
    {
        $this->server()->verifyLogin(
            $nonce = $request->input('nonce'), 
            $request->input('return_url'), 
            $request->input('sig')
        );
        
        $this->cnonce = $nonce;
    }

    /**
     * Handle succesful login request.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function authenticatedServiceProviderLogin(Request $request, $user)
    {
        $returnUrl = $request->input('return_url');

        try {
            $this->server()->login($user);
            $this->server()->saveLoginNonce($this->cnonce, $request);

            $url = $this->server()->getSuccessfulLoginReturnUrl($this->cnonce, $returnUrl);

            return $this->successServiceProviderLogin($request, $user, $url);
        } catch (SSOException $e) {
            $url = $this->server()->getFailedLoginReturnUrl($returnUrl, $this->cnonce, $e->getMessage(), $e->getCode());

            return $this->failedServiceProviderLogin($request, $url, $e);
        }
    }

    /**
     * Handle successful login via service provider.
     * 
     * @param \Illuminate\Http\Request $request
     * @param mixed $user
     * @param string $url
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function successServiceProviderLogin(Request $request, $user, $url)
    {
        return redirect($url);
    }

    /**
     * Handle failed login via service provider.
     * 
     * @param \Illuminate\Http\Request $request
     * @param string $url
     * @param \Dottystyle\LaravelSSO\Exceptions\SSOException $error 
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function failedServiceProviderLogin(Request $request, $url, $error = null)
    {
        return redirect($url);
    }
}