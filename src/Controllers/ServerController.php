<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Controllers;

use Dottystyle\LaravelSSO\IdentityProvider\Concerns\HasServerEndpoints;
use Illuminate\Routing\Controller;

class ServerController extends Controller
{
    use HasServerEndpoints;

    /**
     * Create new instance of the controller.
     * 
     */
    public function __construct() 
    {
        $this->initializeEndpoints();
    }
}