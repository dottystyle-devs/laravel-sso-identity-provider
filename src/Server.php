<?php

namespace Dottystyle\LaravelSSO\IdentityProvider;

use Dottystyle\LaravelSSO\ServiceProviderInterface;
use Dottystyle\LaravelSSO\TokenInterface;
use Dottystyle\LaravelSSO\IdentityProvider\Contracts\ServiceProviderProvider;
use Dottystyle\LaravelSSO\IdentityProvider\Contracts\TokenStore;
use Dottystyle\LaravelSSO\IdentityProvider\Contracts\UserProvider;
use Dottystyle\LaravelSSO\Signature\SignatureAssistant;
use Dottystyle\LaravelSSO\Signature\ServiceProviderSignatureAssistant;
use Dottystyle\LaravelSSO\Signature\ForwardsServiceProviderSignatureCalls;
use Dottystyle\LaravelSSO\Exceptions\AuthenticationException;
use Dottystyle\LaravelSSO\Exceptions\LoginException;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon\Carbon;
use BadMethodCallException;

/**
 * Single sign-on identity provider server.
 *
 * The SSO identity provider server is responsible of managing users sessions for service providers.
 */
class Server
{
    use ForwardsServiceProviderSignatureCalls,
        Concerns\HasNonces;

    const NONCE_MIN_LENGTH = 12;

    /**
     * @var \Illuminate\Container\Container
     */
    protected $app;

    /**
     * @var \Dottystyle\LaravelSSO\IdentityProvider\Contracts\ServiceProviderProvider
     */
    protected $serviceProviders;

    /**
     * @var \Dottystyle\LaravelSSO\ServiceProviderInterface The currently requesting service provider.
     */
    protected $serviceProvider;

    /**
     * @var \Dottystyle\LaravelSSO\Signature\ServiceProviderSignatureAssistant
     */
    protected $sig;

    /**
     * @var \Dottystyle\LaravelSSO\IdentityProvider\Contracts\TokenStore
     */
    protected $tokens;

    /**
     * @var \Dottystyle\LaravelSSO\IdentityProvider\Contracts\UserProvider
     */
    protected $users;

    /**
     * @var \Dottystyle\LaravelSSO\TokenInterface
     */
    protected $token;

    /**
     * @var boolean
     */
    protected $loggedIn = false;

    /**
     * @var array 
     */
    protected $options = [];

    /**
     * Create new instance of the identity provider server.
     *
     * @param \Illuminate\Container\Container $app
     * @param \Dottystyle\LaravelSSO\IdentityProvider\Contracts\ServiceProviderProvider $serviceProviders
     * @param \Dottystyle\LaravelSSO\IdentityProvider\Contracts\UserProvider $users
     * @param \Dottystyle\LaravelSSO\IdentityProvider\Contracts\TokenStore $tokens
     * @param \Dottystyle\LaravelSSO\Signature\SignatureAssistant $sig
     * @param array $options (optional)
     */
    public function __construct(
        $app,
        ServiceProviderProvider $serviceProviders, 
        UserProvider $users,
        TokenStore $tokens,
        SignatureAssistant $sig,
        array $options
    ) {
        $this->app = $app;
        $this->users = $users;
        $this->serviceProviders = $serviceProviders;
        $this->tokens = $tokens;
        $this->sig = new ServiceProviderSignatureAssistant($sig);
        $this->options = $options;

        $this->noncePrefix = 'sso_login:';
        // Use the default cache driver to store nonces
        $this->setNonceRepository($app['cache']->driver());
    }

    /**
     * Get the service provider by its id.
     * 
     * @param mixed $id
     * @return \Dottystyle\LaravelSSO\ServiceProviderInterface
     */
    public function getServiceProvider($id)
    {
        return $this->serviceProviders->get($id);
    }

    /**
     * Get the signature assistant for service provider.
     * 
     * @return \Dottystyle\LaravelSSO\SignatureAssistant\ServiceProviderSignatureAssistant
     */
    public function getSignatureAssistant()
    {
        return $this->sig;
    }

    /**
     * Get the token name.
     * 
     * @return string
     */
    public function getTokenName()
    {
        return $this->options['token'];
    }

    /**
     * Set the token.
     * 
     * @param \Dottystyle\LaravelSSO\TokenInterface $token
     * @return void
     */
    public function setToken(TokenInterface $token)
    {
        $this->token = $token;
    }

    /**
     * Set the token given its token id.
     * 
     * @param string $tokenId
     * @param \Dottystyle\LaravelSSO\Exceptions\AuthenticationException
     * @return void
     */
    public function setTokenById($tokenId)
    {
        // Load the token and check its validity
        if (! ($token = $this->tokens->get($tokenId))) {
            throw new AuthenticationException();
        }

        $this->setToken($token);
    }

    /**
     * Get the current token.
     * 
     * @return \Dottystyle\LaravelSSO\TokenInterface
     * 
     * @throws \Dottystyle\LaravelSSO\Exceptions\AuthenticationException
     */
    public function getToken()
    {
        if (! $this->token) {
            throw new AuthenticationException();
        }
        
        return $this->token;
    }

    /**
     * Check if the token is set.
     * 
     * @return bool
     */
    public function hasToken()
    {
        return ! is_null($this->token);
    }

    /**
     * Get the login nonce key for the given service provider.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     * @param string $cnonce
     * @return string 
     */
    private function getLoginNonceKey(ServiceProviderInterface $sp, $cnonce)
    {
        return $sp->getId().':'.$cnonce;
    }

    /**
     * Save a login nonce for the currently requesting service provider.
     * 
     * @param string $cnonce
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function saveLoginNonce($cnonce, Request $request)
    {
        $data = [
            'ip' => $request->ip(),
            'ua' => $request->header('User-Agent'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ];

        $this->putNonce(
            $this->getLoginNonceKey($this->currentServiceProvider(), $cnonce), $data
        );
    }

    /**
     * Login the authenticatable user (create SSO token and set as cookie).
     * 
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param \Illuminate\Http\Request $request (optional)
     * @return \Dottystyle\LaravelSSO\Contracts\Token
     */
    public function login(Authenticatable $user)
    {
        if ($this->loggedIn) {
            return $this->getToken();
        }

        if (! $this->token) {
            // Issue new SSO token
            $this->setToken($token = $this->generateToken($user));
            $this->app['cookie']->queue($this->getTokenName(), $token->getTokenId(), $token->remainingMinutes());
        }

        // Dispatch an event to let the application do its thing whenever someone logs in.
        // The service provider can be empty/unset here, which is an indication that the login happened locally
        // (not initiated by service provider).
        $this->app['events']->dispatch(new Event\LoginEvent($user, $this->token, $this->currentServiceProvider()));

        $this->loggedIn = true;

        return $this->token;
    }

    /**
     * Generate a global SSO token that will be shared among service providers.
     * 
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @return \Dottystyle\LaravelSSO\TokenInterface
     */
    protected function generateToken($user) 
    {
        $this->tokens->put(
            $token = $this->tokens->create($user)
        );
        
        return $token;
    }

    /**
     * Get the return url of service provider after a successful login.
     * 
     * @param string $nonce The nonce used on login request.
     * @param string $url
     * @param array $params (optional)
     * @return string
     */
    public function getSuccessfulLoginReturnUrl($nonce, $url, array $params = [])
    {
        $token = $this->getToken();

        $params = array_merge([
            'sp' => $this->currentServiceProvider()->getId(),
            'user_id' => $token->getUserId(),
            'token' => $token->getTokenId(),
            'nonce' => $nonce,
            'sig' => $this->signLoginSuccess($token->getTokenId(), $token->getUserId(), $nonce),
            'expires_at' => Carbon::now()->addMinutes($token->remainingMinutes())->getTimestamp(),
            'success' => 1
        ], $params);

        return $this->appendQueryParams($url, $params);
    }

    /**
     * Get the return url of a service provider for a failed login.
     * 
     * @param string $url
     * @param string $nonce
     * @param string $message
     * @param mixed $code (optional)
     * @return string
     */
    public function getFailedLoginReturnUrl($url, $nonce, $message = '', $code = null)
    {
        return $this->appendQueryParams($url, [
            'sig' => $this->signLoginFail($nonce),
            'nonce' => $nonce,
            'success' => 0, 
            'error' => $message,
            'code' => $code
        ]);
    }

    /**
     * Append query parameters to the given url.
     * 
     * @param string $url
     * @param array $params
     * @return string
     */
    protected function appendQueryParams(string $url, array $params) 
    {
        // Preserve any query parameters on the query
        $query = parse_url($url, PHP_URL_QUERY);
        $queryString = http_build_query($params);
        $query = ($query ? '&' : '?').$queryString;

        return $url.$query;
    }

    /**
     * Get the user from the SSO token.
     * 
     * @return \Dottystyle\LaravelSSO\UserInfo
     *
     * @throws \Dottystyle\LaravelSSO\Exceptions\AuthenticationException
     */
    public function getUserInfo()
    {
        $token = $this->getToken();
        $user = $this->users->retrieveById($token->getUserId());

        if (! $user) {
            throw new AuthenticationException();
        }

        // Let others know that user info has been requested
        $this->app['events']->dispatch('sso.user', new Event\GetUserInfoEvent($user, $token, $this->currentServiceProvider()));

        return $user->getUserInfo();
    }

    /**
     * Get the stat/data of the current token.
     * 
     * @return array
     */
    public function getTokenStat()
    {
        return [
            'user' => $this->getUserInfo()->toArray(),
            'expires_at' => $this->getToken()->expired_at->timestamp
        ];
    }

    /**
     * Logout the user.
     * 
     * @param bool $authLogout (optional)
     * @return void
     */
    public function logout($authLogout = true)
    {
        if ($authLogout) {
            $this->app['auth']->logout();
        }

        if ($this->token) {
            $this->tokens->destroy($this->token);
        }

        $this->app['session']->invalidate();
        $this->app['cookie']->queue($this->app['cookie']->forget($this->getTokenName()));
    }

    /**
     * Set the currently requesting service provider.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     * @return void
     */
    public function setCurrentServiceProvider(ServiceProviderInterface $sp)
    {
        $this->serviceProvider = $sp;
        $this->sig->setServiceProvider($sp);

        return $this;
    }

    /**
     * Get the currently requesting service provider.
     * 
     * @return \Dottystyle\LaravelSSO\ServiceProviderInterface
     */
    public function currentServiceProvider()
    {
        return $this->serviceProvider;
    }

    /**
     * Validates the login attempt with the given nonce and signature.
     * 
     * @param string $nonce
     * @param string $returnUrl
     * @param string $signature
     * @return void
     * 
     * @throws \Dottystyle\LaravelSSO\Exceptions\LoginException
     */
    public function verifyLogin($nonce, $returnUrl, $signature)
    {
        if (strlen($nonce) < self::NONCE_MIN_LENGTH) {
            throw new LoginException(
                sprintf('Nonce must be at least %d characters', self::NONCE_MIN_LENGTH)
            );
        }

        $this->sig->verifyLogin($signature, $nonce, $returnUrl);

        // Prevent replay attacks
        if ($this->nonceExists($this->getLoginNonceKey($this->currentServiceProvider(), $nonce))) {
            throw new LoginException();
        }
    }

    /**
     * Verify logout signature.
     * 
     * @param string $signature
     * @return void
     */
    public function verifyLogout($signature)
    {
        $this->ensureToken();
        
        $token = $this->getToken();

        $this->sig->verifyLogout($signature, $token->getTokenId(), $token->getUserId());
    }

    /**
     * Forward some calls to the signature assistant.
     * 
     * @param string $method
     * @param array $arguments
     * @return mixed
     */
    public function __call($method, array $arguments)
    {
        $result = $this->forwardToSignature($this->sig, $method, $arguments, $called);

        if (! $called) {
            throw new BadMethodCallException(sprintf(
                'Call to undefined method %s::%s', get_class($this), $method
            ));
        }

        return $result;
    }
}