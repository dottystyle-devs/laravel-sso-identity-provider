<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Contracts;

use Dottystyle\LaravelSSO\TokenInterface;
use Illuminate\Contracts\Auth\Authenticatable;

interface TokenStore
{
    /**
     * Create a token for the given user.
     * 
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param array $params (optional)
     * @return \Dottystyle\LaravelSSO\TokenInterface $token
     */
    public function create(Authenticatable $user, array $params = []);

    /**
     * Put/save the given token.
     * 
     * @param \Dottystyle\LaravelSSO\TokenInterface $token
     * @return boolean
     */
    public function put(TokenInterface $token);

    /**
     * Get an SSO token by id.
     * 
     * @param string $id
     * @return \Dottystyle\LaravelSSO\TokenInterface
     */
    public function get($id);

    /**
     * Destroy/delete token by id.
     * 
     * @param \Dottystyle\LaravelSSO\TokenInterface $token
     * @return boolean
     */
    public function destroy(TokenInterface $token);
}