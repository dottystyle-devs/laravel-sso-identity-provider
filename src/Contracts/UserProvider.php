<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Contracts;

interface UserProvider
{
    /**
     * Retrieve the user instance by id.
     * 
     * @param mixed $id
     * @return \Dottystyle\LaravelSSO\IdentityProvider\Contracts\UserInterface
     */
    public function retrieveById($id);
}