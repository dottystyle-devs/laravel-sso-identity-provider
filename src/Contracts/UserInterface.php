<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Contracts;

interface UserInterface 
{
    /**
     * Transform the user into a user info instance.
     * 
     * @return \Dottystyle\LaravelSSO\UserInfo
     */
    public function getUserInfo();
}