<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Contracts;

interface ServiceProviderProvider
{
    /**
     * Get the service provider by its id.
     * 
     * @param mixed $id
     * @return \Dottystyle\LaravelSSO\ServiceProviderInterface
     */
    public function get($id);
}