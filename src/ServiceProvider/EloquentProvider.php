<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\ServiceProvider;

use Dottystyle\LaravelSSO\IdentityProvider\Contracts\ServiceProviderProvider;
use Closure;

class EloquentProvider implements ServiceProviderProvider
{
    /**
     * @var string
     */
    protected $model;

    /**
     * @var string 
     */
    protected $attribute;

    /**
     * @var \Closure
     */
    protected $queryCallback;

    /**
     * Create new instance of the service provider using eloquent.
     * 
     * @param string $model
     * @param string $attribute (optional) 
     */
    public function __construct(string $model, string $attribute = null)
    {
        $this->model = $model;
        $this->attribute = $attribute;
    }

    /**
     * Get service provider by its id.
     * 
     * @param mixed $id
     * @return \Dottystyle\LaravelSSO\ServiceProviderInterface
     */
    public function get($id)
    {
        $query = $this->model::query();

        if ($this->queryCallback) {
            $this->queryCallback($query);
        }

        if ($this->attribute) {
            $sp = $query->where($this->attribute, '=', $id)->first();
        } else {
            $sp = $query->find($id);
        }

        return $sp;
    }

    /**
     * Register callback to handle or modify the query to be used when searching for service provider.
     * 
     * @param \Closure $callback
     * @return \Illuminate\Database\Eloquent\Query
     */
    public function withQuery(Closure $callback)
    {
        $this->queryCallback = $callback;

        return $this;
    }
}