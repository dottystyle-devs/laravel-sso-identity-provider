<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\ServiceProvider;

use Illuminate\Support\Manager as Manager;

class ProviderManager extends Manager
{
    /**
     * Provide service providers using eloquent model.
     * 
     * @return \Dottystyle\LaravelSSO\IdentityProvider\ServiceProvider\EloquentProvider
     */
    protected function createEloquentDriver()
    {
        $config = $this->getConfig()['provider'];

        return new EloquentProvider($config['model'], $config['attribute'] ?? null);
    }

    /**
     * Get the configuration for broker provider.
     * 
     * @return array
     */
    protected function getConfig()
    {
        return $this->app['config']['sso.service_provider'];
    }

    /**
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return $this->getConfig()['provider']['driver'];
    }
}