<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Event;

use Dottystyle\LaravelSSO\IdentityProvider\Server;
use Dottystyle\LaravelSSO\IdentityProvider\Concerns\DetectsToken;
use Dottystyle\LaravelSSO\Exceptions\AuthenticationException;

class AuthSubscriber 
{
    use DetectsToken;

    /**
     * @var \Illuminate\Container\Container
     */
    protected $app;

    /**
     * Create new event subscriber.
     * 
     * @param \Illuminate\Container\Container $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     * @return void
     */
    public function subscribe($events)
    {
        $class = get_class($this);

        $events->listen('Illuminate\Auth\Events\Login', "$class@onLogin");
        $events->listen('Illuminate\Auth\Events\Logout', "$class@onLogout");
    }

    /**
     * Handle user login events.
     * 
     * @param mixed $event
     * @return void
     */
    public function onLogin($event)
    {
        $this->server()->login($event->user);
    }

    /**
     * Handle user logout events.
     * 
     * @param mixed $event
     * @return void
     */
    public function onLogout($event)
    {
        try {
            // Load the token from request since the middleware is not trigger on default logout route.
            $this->setTokenFromRequest($this->server(), $this->app['request']);
        } catch (AuthenticationException $e) {
        } finally {
            $this->server()->logout(false);
        }
    }

    /**
     * Get the server from container.
     * 
     * @return \Dottystyle\LaravelSSO\IdentityProvider\Server
     */
    protected function server()
    {
        return $this->app[Server::class];
    }
}
