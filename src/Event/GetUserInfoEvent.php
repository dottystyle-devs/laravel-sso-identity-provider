<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Event;

use Dottystyle\LaravelSSO\IdentityProvider\Contracts\UserInterface;
use Dottystyle\LaravelSSO\TokenInterface;
use Dottystyle\LaravelSSO\ServiceProviderInterface;

class GetUserInfoEvent
{
    use EventHelpers;

    /**
     * Create a new event instance.
     * 
     * @param \Dottystyle\LaravelSSO\IdentityProvider\Contracts\UserInterface $user
     * @param \Dottystyle\LaravelSSO\TokenInterface $token
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     */
    public function __construct(UserInterface $user, TokenInterface $token, ServiceProviderInterface $sp) 
    {
        $this->user = $user;
        $this->token = $token;
        $this->serviceProvider = $sp;
    }
}