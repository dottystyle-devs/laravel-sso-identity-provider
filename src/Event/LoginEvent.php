<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Event;

use Dottystyle\LaravelSSO\ServiceProviderInterface;
use Dottystyle\LaravelSSO\TokenInterface;
use Illuminate\Contracts\Auth\Authenticatable;

class LoginEvent
{
    use EventHelpers;

    /**
     * Create a new login event instance.
     * 
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param \Dottystyle\LaravelSSO\TokenInterface $token
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp (optional)
     * @return 
     */
    public function __construct(Authenticatable $user, TokenInterface $token, ServiceProviderInterface $sp = null) 
    {
        $this->user = $user;
        $this->token = $token;
        $this->serviceProvider = $sp;
    }
}