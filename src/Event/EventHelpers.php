<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Event;

trait EventHelpers
{
    /**
     * @var mixed
     */
    public $user;

    /**
     * @var \Dottystyle\LaravelSSO\TokenInterface
     */
    public $token;

    /**
     * @var \Dottystyle\LaravelSSO\ServiceProviderInterface
     */
    public $serviceProvider;
}