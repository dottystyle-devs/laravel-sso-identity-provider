<?php

namespace Dottystyle\LaravelSSO\IdentityProvider;

use Dottystyle\LaravelSSO\IdentityProvider\ServiceProvider\ProviderManager;
use Dottystyle\LaravelSSO\IdentityProvider\ServiceProvider\ServiceProviderProvider;
use Dottystyle\LaravelSSO\Signature\SignatureAssistant;
use Dottystyle\LaravelSSO\Signature\HMACSignatureAssistant;
use Illuminate\Cookie\Middleware\EncryptCookies;
use Illuminate\Support\ServiceProvider;

class SSOServiceProvider extends ServiceProvider
{
    /**
     * Register any application authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $root = dirname(__DIR__);

        $this->publishes([ "$root/config.php" => config_path('sso.php') ]);
        $this->mergeConfigFrom("$root/config.php", 'sso');

        $this->loadRoutesFrom("$root/routes.php");
        $this->loadMigrationsFrom("$root/migrations");
        $this->loadViewsFrom("$root/resources/views", 'sso');
        
        $this->registerMiddlewares();
        $this->registerEventSubscribers();
        $this->excludeTokensFromEncryption();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerServer();
        $this->registerTokens();
    }

    /**
     * Register the identity provider singleton.
     * 
     * @return void
     */
    protected function registerServer()
    {
        // Register the SSO identity provider instance
        $this->app->singleton('sso.server', function ($app) {
            return new Server($app,
                (new ProviderManager($app))->driver(),
                new AuthGuardUserProvider($app->make('auth')->guard()),
                $app->make(Contracts\TokenStore::class),
                $app->make(SignatureAssistant::class),
                ['token' => $app['config']['sso.token']]
            );
        });

        $this->app->alias('sso.server', Server::class);

        // Register the signature assistant in generating and verifying signatures
        $this->app->singleton(SignatureAssistant::class, function () {
            return new HMACSignatureAssistant();
        });
    }

    /**
     * Register the token store.
     * 
     * @return void
     */
    protected function registerTokens()
    {
        $this->app->singleton(Contracts\TokenStore::class, function ($app) {
            $config = $app['config'];

            // Create first the identity provider instance then return the token store
            return new Token\Store($config['sso.token_model'], $config['sso.token_lifetime']);
        });
    }

    /**
     * Register our middleware.
     * 
     * @return void
     */
    protected function registerMiddlewares()
    {
        $this->app['router']->aliasMiddleware('sso.auth', Middleware\Authenticate::class);
        $this->app['router']->aliasMiddleware('sso.token_auth', Middleware\AuthenticateToken::class);
    }

    /**
     * Register the subscriber class.
     *
     * @return void
     */
    protected function registerEventSubscribers()
    {
        $this->app->singleton(Event\AuthSubscriber::class, function ($app) {
            return new Event\AuthSubscriber($app);
        });

        $this->app['events']->subscribe(Event\AuthSubscriber::class);
    }

    /**
     * Exclude the token cookie from being encrypted by the middleware.
     * 
     * @return void
     */
    protected function excludeTokensFromEncryption()
    {
        $this->app->resolving(EncryptCookies::class, function ($middleware) {
            $middleware->disableFor($this->app->config['sso.token']);
        });
    }

    /**
     * Get the services provided.
     * 
     * @return string
     */
    public function provides()
    {
        return ['sso.server', Server::class];
    }
}