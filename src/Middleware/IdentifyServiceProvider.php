<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Middleware;

use Dottystyle\LaravelSSO\IdentityProvider\Server;
use Dottystyle\LaravelSSO\Exceptions\SSOException;
use Closure;

class IdentifyServiceProvider
{
    /**
     * @var \Dottystyle\LaravelSSO\IdentityProvider\Server
     */
    protected $server;

    /**
     * Create new instance of the middleware.
     * 
     * @param \Dottystyle\LaravelSSO\IdentityProvider\Server $server
     */
    public function __construct(Server $server)
    {
        $this->server = $server;
    }

    /**
     * Handle the request.
     * Identifies the requesting service provider from the request and sets it on the server.
     * 
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return void
     */
    public function handle($request, Closure $next)
    {
        if (! ($sp = $this->resolveServiceProvider($request))) {
            throw new SSOException("Invalid or missing service provider from request");
        }

        $this->server->setCurrentServiceProvider($sp);

        return $next($request);
    }

    /**
     * Resolve the service provider instance the from the request.
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Dottystyle\LaravelSSO\ServiceProviderInterface
     */
    protected function resolveServiceProvider($request)
    {
        // Check first the header then the request parameters or body for the service provider's identifier.
        return $this->server->getServiceProvider(
            $request->header('X-SSO-SP') ?: $request->input('sp')
        );
    }
}