<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Middleware;

use Dottystyle\LaravelSSO\IdentityProvider\Server;
use Dottystyle\LaravelSSO\IdentityProvider\Concerns\DetectsToken;
use Dottystyle\LaravelSSO\Exceptions\AuthenticationException;
use Illuminate\Auth\AuthenticationException as LaravelAuthenticationException;
use Closure;

class AuthenticateToken
{
    use DetectsToken;

    /**
     * Create new instance of the middleware.
     * 
     * @param \Dottystyle\LaravelSSO\IdentityProvider\Server $server
     */
    public function __construct(Server $server)
    {
        $this->server = $server;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request  $request
     * @param \Closure $next
     * @param bool $throw (optional)
     * @return mixed
     */
    public function handle($request, Closure $next, $throw = true)
    {
        try {
            // Authenticate SSO token then perform local authentication check.
            $this->setTokenFromRequest($this->server, $request);

            // Let logout requests passthru.
            if ($request->route()->getName() === 'sso.logout') {
                return $next($request);
            }

            return $next($request);
        } catch (AuthenticationException $e) {
            if ((bool) $throw) {
                throw new LaravelAuthenticationException();
            }

            return $next($request);
        }
    }
}