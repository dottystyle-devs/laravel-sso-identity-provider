<?php

namespace Dottystyle\LaravelSSO\IdentityProvider\Middleware;

use Dottystyle\LaravelSSO\IdentityProvider\Server;
use Dottystyle\LaravelSSO\TokenInterface;
use Dottystyle\LaravelSSO\Exceptions\AuthenticationException;
use Dottystyle\LaravelSSO\Exceptions\MismatchedUsersException;
use Dottystyle\LaravelSSO\IdentityProvider\Concerns\DetectsToken;
use Illuminate\Auth\Middleware\Authenticate as BaseAuthenticateMiddleware;
use Illuminate\Auth\AuthenticationException as LaravelAuthenticationException;
use Illuminate\Contracts\Auth\Factory as Auth;
use Closure;

class Authenticate extends BaseAuthenticateMiddleware
{
    use DetectsToken;

    /**
     * @var \Dottystyle\LaravelSSO\IdentityProvider\Server $server
     */
    protected $server;

    /**
     * Create new instance of the middleware.
     * 
     * @param \Dottystyle\LaravelSSO\IdentityProvider\Server $server
     * @param \Illuminate\Contracts\Auth\Factory $auth
     */
    public function __construct(Server $server, Auth $auth)
    {
        $this->server = $server;
       
        parent::__construct($auth);
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request  $request
     * @param \Closure $next
     * @param string[] ...$guards
     * @return mixed
     */
    public function handle($request, Closure $next, ...$guards)
    {
        try {
            // Authenticate SSO token then perform local authentication check.
            $this->setTokenFromRequest($this->server, $request);

            $this->authenticate($request, $guards);
            $this->validateSameUser($this->server->getToken());

            return $next($request);
        } catch (AuthenticationException $e) {
            // We will forcefully logout the user even if the user is locally authenticated
            // since the token preceeds the local authentication.
            $this->authenticate($request, $guards);
            $this->logout($request, $guards);
        } catch (LaravelAuthenticationException $e) {
            // This exception will be thrown after authenticating the token which means the token is valid 
            // but the user is not logged in locally. Manually login the user using its id.
            $this->auth->loginUsingId($this->server->getToken()->getUserId());

            return $next($request);
        } catch (MismatchedUsersException $e) {
            // User of token doesn't match with the currently authenticated user.
            // In such cases, we think it is better to ask the user to authenticate itself again.
            $this->logout($request, $guards);
        }
    }

    /**
     * Log the user out of the application.
     * Clears the SSO token from cookies.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param array $guards
     * @return void
     */
    protected function logout($request, array $guards)
    {
        $this->server->logout();

        throw new LaravelAuthenticationException(
            'Unauthenticated.', $guards, $this->redirectTo($request)
        );
    }

    /**
     * Validates the auth user id is the same with the token user id.
     * 
     * @param \Dottystyle\LaravelSSO\TokenInterface $token
     * @return void
     * 
     * @throws \Dottystyle\LaravelSSO\Exceptions\MismatchedUserAndTokenException
     */
    protected function validateSameUser(TokenInterface $token)
    {
        if ($token->getUserId() !== $this->auth->id()) {
            throw new MismatchedUsersException();
        }
    }
}