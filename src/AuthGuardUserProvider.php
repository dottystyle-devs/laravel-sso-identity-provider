<?php

namespace Dottystyle\LaravelSSO\IdentityProvider;

use Dottystyle\LaravelSSO\IdentityProvider\Contracts\UserProvider;
use Illuminate\Contracts\Auth\Guard;
use InvalidArgumentException;

/**
 * Provides users from the user provider of the default authentication guard.
 */
class AuthGuardUserProvider implements UserProvider 
{
    /**
     * @var \Illuminate\Contracts\Auth\UserProvider
     */
    protected $provider; 

    /**
     * Create new instance of the user provider.
     * 
     * @param \Illuminate\Contracts\Auth\Guard $guard
     */
    public function __construct(Guard $guard) 
    {
        // Must use the GuardHelpers trait or implement methods
        if (! method_exists($guard, 'getProvider')) {
            throw new InvalidArgumentException("Auth guard must implement getProvider method");
        }

        $this->provider = $guard->getProvider();
    }

    /**
     * Retrieve the user info by id.
     * 
     * @param mixed $id
     * @return \Dottystyle\LaravelSSO\IdentityProvider\Contracts\UserInterface
     */
    public function retrieveById($id) 
    {
        return $this->provider->retrieveById($id);
    }
}