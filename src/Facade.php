<?php

namespace Dottystyle\LaravelSSO\IdentityProvider;

use Illuminate\Support\Facades\Facade as BaseFacade;

class Facade extends BaseFacade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'sso.server';
    }

    /**
     * Register SSO server login routes using the typical or specified controller.
     * 
     * @param string $controller (optional)
     * @return void
     */
    public static function authRoutes($controller)
    {
        $router = static::$app->make('router');

        $router->as('sso.')
            ->middleware('sso.token_auth:0')
            ->group(function () use ($router, $controller) {
                $router->get('sso/login', "{$controller}@showSSOLoginFormAction")->name('login_form');
                $router->post('sso/login', "{$controller}@ssoLogin")->name('login');
            });
    }
}