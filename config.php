<?php

return [
    /**
     * @var string The name of the token
     */
    'token' => env('SSO_TOKEN', 'sso_token'),

    /**
     * @var int 
     */
    'token_lifetime' => env('SSO_TOKEN_LIFETIME', 60 * 24 * 60),

    /**
     * @var string The eloquent model class for tokens
     */
    'token_model' => Dottystyle\LaravelSSO\IdentityProvider\Token\Model::class,

    /**
     * Contains configuration for service providers
     */
    'service_provider' => [
        /**
         * The provider of service providers
         */
        'provider' => [
            'driver' => 'eloquent',
            'model' => '',
            'attribute' => ''
        ]
    ]
];